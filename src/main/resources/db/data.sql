--CATEGORIA CUARTO
INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual', 'Ideal para quienes viajan solos.',50.0);

INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual 1', 'Ideal para quienes viajan solos pero con mayor presupuesto.',150.0);

INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual 2', 'Ideal para quienes viajan solos comodamente.',350.0);
--CUARTO
INSERT INTO cuarto (numero, descripcion,categoria)
    VALUES(1,'Vista a la piscina',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(2,'Remodelado recientemente',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(3,'Cuarto tradicional tipo 1',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(4,'Cuarto tradicional tipo 2',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(5,'Cuarto tradicional tipo 3',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(6,'Ejecutivo Nivel 1',3);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(7,'Ejecutivo Nivel 2',2);
-- HUESPED
INSERT INTO huesped(nombre, email, telefono)
SELECT 'Jerry Calero', 'yitzahksoft@gmail.com', '84865703'
UNION ALL
SELECT 'Anderson Calero', 'andercalero@gmail.com', '84865703'
UNION ALL
SELECT 'Mavis Calero', 'maviscalero@gmail.com', '84865703';

-- RESERVACION
INSERT INTO reservacion(desde, hasta, cuarto, huesped)
SELECT '2017-02-01', '2017-02-05', 1, 1
union all
SELECT '2017-02-05', '2017-02-07', 2, 3
union all
SELECT '2017-02-02', '2017-02-06', 3, 2
union all 
SELECT '2017-02-21' , '2017-02-28', 1, 1;
