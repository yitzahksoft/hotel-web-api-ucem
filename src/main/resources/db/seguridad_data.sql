--https://www.dailycred.com/article/bcrypt-calculator 
--base 10
--1234 pass
--https://bcrypt-generator.com/
INSERT INTO usuarios(username,password,enabled)
VALUES ('ucem','$2a$10$qHt9M7kgjZMBp/MsSk5Q3OVaO5JCTmyrP7/6WDwcAaKeQ1IGrt1Rq', true);
INSERT INTO usuarios(username,password,enabled)
VALUES ('jcalero','$2a$10$qHt9M7kgjZMBp/MsSk5Q3OVaO5JCTmyrP7/6WDwcAaKeQ1IGrt1Rq', true);

INSERT INTO usuarios_roles (username, role)
VALUES ('ucem', 'ROLE_ADMIN');-- el rol ROLE_ADMIN son valores ya definidos

INSERT INTO usuarios_roles (username, role)
VALUES ('jcalero', 'ROLE_USER');