package ni.edu.ucem.webapi.serviceImpl;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ni.edu.ucem.webapi.dao.CategoriaCuartoDAO;
import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.dao.HuespedDAO;
import ni.edu.ucem.webapi.dao.ReservacionDAO;
//import ni.edu.ucem.webapi.dao.CupoDAO;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Paginacion;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.ReservacionCompuesta;
import ni.edu.ucem.webapi.service.InventarioService;

@Service
public class InventarioServiceImpl implements InventarioService 
{
    private final CategoriaCuartoDAO categoriaCuartoDAO;
    private final CuartoDAO cuartoDAO;
    private final HuespedDAO huespedDAO;
    private final ReservacionDAO reservacionDAO;
    
    public InventarioServiceImpl(final CategoriaCuartoDAO categoriaCuartoDAO,
            final CuartoDAO cuartoDAO, final HuespedDAO huespedDAO
            , final ReservacionDAO reservacionDAO)
    {
        this.categoriaCuartoDAO = categoriaCuartoDAO;
        this.cuartoDAO = cuartoDAO;
        this.huespedDAO = huespedDAO;
        this.reservacionDAO = reservacionDAO;
    }
    
    @Transactional
    @Override
    public void agregarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto) 
    {
        this.categoriaCuartoDAO.agregar(pCategoriaCuarto);
    }

    /*@Transactional
    @Override
    public void guardarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto) 
    {
        if(pCategoriaCuarto.getId() < 1)
        {
            throw new IllegalArgumentException("La categoría del cuarto no existe");
        }
        this.categoriaCuartoDAO.guardar(pCategoriaCuarto);
    }*/

    @Transactional
    @Override
    public void eliminarCategoriaCuarto(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.categoriaCuartoDAO.eliminar(pId);
    }

    @Override
    public CategoriaCuarto obtenerCategoriaCuarto(final int pId) 
    {
        return this.categoriaCuartoDAO.obtenerPorId(pId);
    }

   
    /*public List<CategoriaCuarto> obtenerTodosCategoriaCuartos() 
    {
        return this.categoriaCuartoDAO.obtenerTodos();
    }*/
    @Override
    public Pagina<CategoriaCuarto> obtenerTodosCategoriaCuartos(Paginacion paginacion, final String pSort, final String pSortOrder, final String pSearch, String pFields) 
    {
        List<CategoriaCuarto> categoriaCuarto;
        final int count = this.categoriaCuartoDAO.contar(pSearch);
        if(count > 0)
        {
            categoriaCuarto = this.categoriaCuartoDAO.obtenerTodos(paginacion.getOffset()
                    ,paginacion.getLimit(), pSort, pSortOrder, pSearch, pFields);
            
        }
        else
        {
            categoriaCuarto = new ArrayList<CategoriaCuarto>();
        }
        return new Pagina<CategoriaCuarto>(categoriaCuarto, count,  paginacion.getOffset(), paginacion.getLimit());
    }
    
    
    @Transactional
    @Override
    public void agregarCuarto(final Cuarto pCuarto) 
    {
        this.cuartoDAO.agregar(pCuarto);
    }
    
    @Transactional
    @Override
    public void guardarCuarto(final Cuarto pCuarto) 
    {
        if(pCuarto.getId() < 1)
        {
            throw new IllegalArgumentException("El cuarto no existe");
        }
        this.cuartoDAO.guardar(pCuarto);
    }
    
    @Transactional
    @Override
    public void eliminarCuarto(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.cuartoDAO.eliminar(pId);
    }

    @Override
    public Cuarto obtenerCuarto(final int pId) 
    {
        if (pId < 0) 
        {
            throw new IllegalArgumentException("ID inválido. debe ser mayor a cero.");
        }
        return this.cuartoDAO.obtenerPorId(pId); 
    }

    @Override
    public Pagina<Cuarto> obtenerTodosCuarto(Paginacion paginacion, final String pSort, final String pSortOrder, final String pSearch, String pFields) 
    {
        List<Cuarto> cuartos;
        final int count = this.cuartoDAO.contar(pSearch);
        if(count > 0)
        {
            cuartos = this.cuartoDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit(), pSort, pSortOrder, pSearch, pFields);
        }
        else
        {
            cuartos = new ArrayList<Cuarto>();
        }
        return new Pagina<Cuarto>(cuartos, count,  paginacion.getOffset(), paginacion.getLimit());
    }

    @Override
    public Pagina<Cuarto> obtenerTodosCuartoEnCategoria(final int pCategoriaCuartoId, final Paginacion paginacion)
    {
        final int count = this.cuartoDAO.contarPorCategoria(pCategoriaCuartoId);
        List<Cuarto> cuartos = null;
        if(count > 0)
        {
            cuartos = this.cuartoDAO.obtenerTodosPorCategoriaId(pCategoriaCuartoId, paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
            cuartos = new ArrayList<Cuarto>();
        }
        return new Pagina<Cuarto>(cuartos, count,  paginacion.getOffset(), paginacion.getLimit());
    }

    @Override
    public Cupo obtenerCuartosDisponibles(final String pSearch, final Date fechaI, final Date fechaF) {
       
        Cupo cupo = new Cupo();
              
        List<Cuarto> cuartos;
        //final int count = this.cuartoDAO.contar(pSearch);
        
        cuartos = this.cuartoDAO.obtenerCuartosDisponibles(pSearch);
       
        //if  (count > 0){
            cupo.setFechaIngreso(fechaI);
            cupo.setFechaSalida(fechaF);
            cupo.setCuartos(cuartos);
        //}
     
        return cupo;
    }

    @Override
    public ReservacionCompuesta obtenerReservacionCompuesta(final int id_reservacion) {
        Cuarto cuartos;
        Huesped huesped;
        Reservacion reservacion;
        
        ReservacionCompuesta reservacionCompuesta = new ReservacionCompuesta();
         
        reservacion = this.reservacionDAO.obtenerPorId(id_reservacion);
        
        cuartos = this.cuartoDAO.obtenerPorId(reservacion.getCuarto()) ; 
        
        huesped = this.huespedDAO.obtenerHuespedPorId(reservacion.getHuesped()); 
                      
        reservacionCompuesta.setCuarto(cuartos);
        reservacionCompuesta.setHuesped(huesped);
        reservacionCompuesta.setDesde(reservacion.getDesde());
        reservacionCompuesta.setHasta(reservacion.getHasta());
        reservacionCompuesta.setId(reservacion.getId());
        return reservacionCompuesta;
    }

    @Override
    public Huesped obtenerHuespedPorId(int pId) {
        if (pId < 0) 
        {
            throw new IllegalArgumentException("ID inválido. debe ser mayor a cero.");
        }
        return this.huespedDAO.obtenerHuespedPorId(pId); 
    }

    @Transactional
    @Override
    public Integer guardarReservacion(Reservacion pReservacion) {
        return this.reservacionDAO.agregar(pReservacion); 
    }

        
}
