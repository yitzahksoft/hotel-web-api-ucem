/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.dao;

import ni.edu.ucem.webapi.modelo.Huesped;

/**
 *
 * @author jcalero
 */
public interface HuespedDAO 
{
    public Huesped obtenerHuespedPorId(final int pId);
}
