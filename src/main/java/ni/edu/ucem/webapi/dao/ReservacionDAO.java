/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.dao;

import java.util.List;
import ni.edu.ucem.webapi.modelo.Reservacion;

public interface ReservacionDAO {
    public Reservacion obtenerPorId(final int pId);

    public int contar(final String pSearch);
    
    public List<Reservacion> obtenerTodos(final int offset, final int limit, final String sort, final String sortOrder, final String search, String pFields);

    public List<Reservacion> obtenerTodosDisponible(final int offset, final int limit, final String sort, final String sortOrder, final String search, String pFields);
    
    public Integer agregar(final Reservacion pReservacion);

    public void guardar(final Reservacion pReservacion);

    public void eliminar(final int pId);
}
