package ni.edu.ucem.webapi.dao;

import java.util.List;

import ni.edu.ucem.webapi.modelo.CategoriaCuarto;

public interface CategoriaCuartoDAO 
{
    public CategoriaCuarto obtenerPorId(final int pId);

    //public List<CategoriaCuarto> obtenerTodos();
    
    public List<CategoriaCuarto> obtenerTodos(final int offset, final int limit, final String sort, final String sortOrder, final String search, String pFields);

    public void agregar(final CategoriaCuarto pCategoriaCuarto);

    public void guardar(final CategoriaCuarto pCategoriaCuarto);

    public void eliminar(final int pId);
    
    public int contar(final String pSearch);
}
