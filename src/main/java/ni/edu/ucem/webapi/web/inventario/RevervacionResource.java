/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.web.inventario;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.validation.Valid;
import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.ReservacionCompuesta;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jcalero
 */
@RestController
@RequestMapping("/v1/reservaciones")
public class RevervacionResource {
    
     private final InventarioServiceImpl inventarioService;
    
    @Autowired
    public RevervacionResource(final InventarioServiceImpl inventarioService)
    {
        this.inventarioService = inventarioService;
    }
    
   @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
   public ApiResponse obtener(@PathVariable("id") final int id)
    {
      final ReservacionCompuesta reservacionCompuesta = this.inventarioService.obtenerReservacionCompuesta(id);
     /* if (webRequest.checkNotModified(cuarto.getModificado().getTime())) {
           return null;
        } */
        return new ApiResponse(ApiResponse.Status.OK, reservacionCompuesta);
    }
    
    @RequestMapping(method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    
    public ApiResponse guardarReservacion(@Valid @RequestBody final Reservacion reservacion, BindingResult result) throws ParseException 
    {
        Integer id;
         SimpleDateFormat parseadorFechaActual = new SimpleDateFormat("yyyy-MM-dd");
         SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
         Date fechaActal = parseadorFechaActual.parse(formateador.format(new Date()));
         
        if (fechaActal.compareTo(reservacion.getDesde()) > 0){
             throw new IllegalArgumentException("404 Bad Request la fecha de Ingreso es menor a la fecha actual ");
         }

         if (fechaActal.compareTo(reservacion.getHasta()) > 0){
            throw new IllegalArgumentException("404 Bad Request la fecha de Salida es menor a la fecha actual ");
         }

         if (reservacion.getHasta().compareTo(reservacion.getDesde()) < 0){
            throw new IllegalArgumentException("404 Bad Request la fecha de Salida es menor a la fecha Ingreso ");
         }
         
        if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }
        
        id = this.inventarioService.guardarReservacion(reservacion);
        if (id ==0){
            throw new IllegalArgumentException("404 Bad Request El cuarto ya esta reservado en esta fecha ");
        } else {
            final ReservacionCompuesta reservacionCompuesta = this.inventarioService.obtenerReservacionCompuesta(id);
            return new ApiResponse(ApiResponse.Status.OK, reservacionCompuesta);
        }
        
    }
    
   
}
