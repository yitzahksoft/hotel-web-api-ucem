package ni.edu.ucem.webapi.web.inventario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Paginacion;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import static ni.edu.ucem.webapi.web.inventario.CuartoResource.isNumeric;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/v1/inventario/categorias")

public class CategoriaResource {

    private final InventarioServiceImpl inventarioService;

    @Autowired
    public CategoriaResource(final InventarioServiceImpl inventarioService) {
        this.inventarioService = inventarioService;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ListApiResponse<CategoriaCuarto> obtenerCategoriaCuarto(
          @RequestParam(value = "fields", required = false, defaultValue="")  String fields,
            @RequestParam(value = "nombre", required = false, defaultValue="")  String nombre,
            @RequestParam(value = "descripcion", required = false, defaultValue="") final String descripcion,
            @RequestParam(value = "precio", required = false, defaultValue="")  String precio,
            @RequestParam(value = "search", required = false, defaultValue="")  String search,
            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="0") final Integer limit,
            @RequestParam(value = "sort", required = false, defaultValue="")  String sort,
            @RequestParam(value = "sortOrder", required = false, defaultValue="")  String sortOrder)
           {
       // final List<CategoriaCuarto> categoriaCuarto;
        
         final Paginacion paginacion = new Paginacion.Builder(offset, limit).build();
         String whereFilter = " where 1 = 1 ";  
         Pagina<CategoriaCuarto> pagina;
         String[] campos = fields.split(",");
         String columnas = "";
         int entra = 0;
           
         if (!"".equals(precio) && isNumeric(precio)){
             whereFilter = whereFilter + " and precio = " + precio;
         }

         if (!"".equals(nombre)){
             whereFilter = whereFilter + " and nombre = '" + nombre + "'";
         } 

          if (!"".equals(descripcion)){
             whereFilter = whereFilter + " and descripcion = '" + descripcion + "'";
         }
         
         if (!"".equals(search)){
             whereFilter = whereFilter +  " and ( descripcion like '%" + search + "%' or nombre like '%" + search + "%'";

             if (isNumeric(search)) {
                  whereFilter = whereFilter +  " or precio = " + search;
             }
              whereFilter = whereFilter +  ")";
         }

         if (!"nombre".equals(sort) && !"descripcion".equals(sort) && !"precio".equals(sort) && !"".equals(sort)){
             //throw new IllegalArgumentException("404 (Not Found) campo no encontado para " + sort);
              sort = "1";
         } 
         if ("".equals(sort) ) {
             sort = "1";
         } 

         if (!"asc".equals(sortOrder) && !"desc".equals(sortOrder) && !"".equals(sortOrder)) {
             //throw new IllegalArgumentException("404 (Not Found) parametro no encontrado para " + sortOrder);
              sortOrder = "asc";
         }

         if ("".equals(sortOrder) ) {
             sortOrder = "asc";
         } 
        
         if (campos.length <= 0) {
            columnas = " * ";
        }

        for (int i = 0; i < campos.length; i++){
         //System.out.println("campos[i] = " + campos[i]);
            if ("id".equals(campos[i]) || "nombre".equals(campos[i]) ||  "descripcion".equals(campos[i]) || "precio".equals(campos[i]) ) {
                if (entra == 1) {
                   columnas = columnas + " , "; 
                }
                entra = 1;
                columnas = columnas + campos[i];
            }
        }

         if (campos.length <= 0 || entra == 0) {
            columnas = " * ";
        }
         
        pagina = this.inventarioService.obtenerTodosCategoriaCuartos(paginacion, sort, sortOrder, whereFilter, columnas);

        return new ListApiResponse<CategoriaCuarto>(Status.OK, pagina);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ApiResponse obtener(@PathVariable("id") final int id) {
        try {
            final CategoriaCuarto categoriaCuarto = this.inventarioService.obtenerCategoriaCuarto(id);
            return new ApiResponse(Status.OK, categoriaCuarto);
        } catch (EmptyResultDataAccessException e) {
            return new ApiResponse(Status.BAD_REQUEST, "No exite un categoria con el ID" + id);
        }
    }

    @RequestMapping(method = RequestMethod.POST,
            produces = "application/json")
    public ApiResponse guardarCategoria(@RequestBody final CategoriaCuarto categoriaCuarto) {
        this.inventarioService.agregarCategoriaCuarto(categoriaCuarto);
        return new ApiResponse(Status.OK, categoriaCuarto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT,
            produces = "application/json")
    public ApiResponse guardarCategoria(@PathVariable("id") final int id,
            @RequestBody final CategoriaCuarto categoriaCuartoActualizado) {
        final CategoriaCuarto categoriaCuarto = new CategoriaCuarto(id,
                categoriaCuartoActualizado.getDescripcion(),
                categoriaCuartoActualizado.getNombre(),
                categoriaCuartoActualizado.getPrecio());
        this.inventarioService.agregarCategoriaCuarto(categoriaCuarto);
        return new ApiResponse(Status.OK, categoriaCuarto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE,
            produces = "application/json")
    public ApiResponse eliminarCuarto(@PathVariable("id") final int id) {
        try {
            final CategoriaCuarto categoriaCuarto = this.inventarioService.obtenerCategoriaCuarto(id);
            this.inventarioService.eliminarCategoriaCuarto(categoriaCuarto.getId());
            return new ApiResponse(Status.OK, null);
        } catch (EmptyResultDataAccessException e) {
            return new ApiResponse(Status.BAD_REQUEST, "No exite un categoriaCuarto con el ID " + id);
        }
    }
}
