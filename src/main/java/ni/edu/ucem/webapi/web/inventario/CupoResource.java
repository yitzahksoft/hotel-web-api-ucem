/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.web.inventario;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import static ni.edu.ucem.webapi.web.inventario.CuartoResource.isNumeric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/disponibilidad/cupos")
public class CupoResource {
    
    private final InventarioServiceImpl inventarioService;
      
    public static boolean isDateValid(String date) 
        {
            try {
                    DateFormat df = new SimpleDateFormat("yyyyMMdd");
                    df.setLenient(false);
                    df.parse(date);
                    return true;
                } catch (ParseException e) {
                    return false;
                }
        }
    
    
    @Autowired
     public CupoResource(final InventarioServiceImpl inventarioService)
    {
        this.inventarioService = inventarioService;
    }
    
    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtenerCuartos(
            @RequestParam(value = "fechaIngreso", required = true)  String fechaIngreso,
            @RequestParam(value = "fechaSalida", required = true)  String fechaSalida,
            @RequestParam(value = "categoria", required = false, defaultValue="") final String categoria,
            @RequestParam(value = "offset", required = false, defaultValue ="") final String offset,
            @RequestParam(value = "limit", required = false, defaultValue="") final String limit
            ) throws ParseException
            
    {
        
        
        
// el que parsea
        SimpleDateFormat parseador = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat parseadorFechaActual = new SimpleDateFormat("yyyy-MM-dd");
        // el que formatea
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        
        Date df = null;
        Date di = null;
        Date fechaActal = parseadorFechaActual.parse(formateador.format(new Date()));
        
        String whereFilter = " where id not in (select cuarto from reservacion where desde >= '" ;
               
        System.out.println("fechaActal = " + fechaActal + " parseador.parse(fechaIngreso) = "+ parseador.parse(fechaIngreso));
       
        /*
        
         String mensaje = "********** Error **********\n" +
"\n" +
"ERROR: No se puede anular ya que tiene un saldo a favor aplicado.\n" +
"SQL state: P0001\n" +
"Context: SQL statement \"SELECT sp_anular_nota_credito(v_n_id_documento , p_motivo_anulacion, p_c_usuario)\"\n" +
"PL/pgSQL function sp_anular_devoluciones_nc(integer,character varying,character varying,integer) line 97 at PERFORM";
        
         System.out.printf("Mensaje"  + mensaje);
        mensaje = mensaje.substring(mensaje.indexOf(":"));
        mensaje = mensaje.substring(0, mensaje.indexOf("."));
        System.out.printf("Mensaje"  + mensaje);
        
        
        */
        
        
        
        
        
        if (isDateValid(fechaIngreso) && isDateValid(fechaSalida)) {
            if (fechaActal.compareTo(parseador.parse(fechaIngreso)) > 0){
                throw new IllegalArgumentException("404 Bad Request la fecha de Ingreso es menor a la fecha actual ");
            }
                      
            if (fechaActal.compareTo(parseador.parse(fechaSalida)) > 0){
               throw new IllegalArgumentException("404 Bad Request la fecha de Salida es menor a la fecha actual ");
            }
            
            if (parseador.parse(fechaSalida).compareTo(parseador.parse(fechaIngreso)) < 0){
               throw new IllegalArgumentException("404 Bad Request la fecha de Salida es menor a la fecha Ingreso ");
            }
                di = parseador.parse(fechaIngreso);
                whereFilter = whereFilter +  formateador.format(di) + "'";
                                 
                df = parseador.parse(fechaSalida);
                whereFilter = whereFilter + " and hasta <= '" +  formateador.format(df) + "')";
                 
        }else {
             throw new IllegalArgumentException("404 Bad Request. Los parametros fechaIngreso y fechaSalida son requeridos con formato yyyyMMdd ");
        }
        
        
        if (!"".equals(categoria) && isNumeric(categoria)){
             whereFilter = whereFilter + " and categoria = " + categoria;
        }
                
        if (!"".equals(offset) && isNumeric(offset) && !"".equals(limit) && isNumeric(limit)){
             whereFilter = whereFilter + " OFFSET " + offset + " limit " + limit ;
        }
         //System.out.println("whereFilter = " + whereFilter);
                
        final Cupo cupo =  this.inventarioService.obtenerCuartosDisponibles(whereFilter, di, df);
               
        return new ApiResponse(ApiResponse.Status.OK, cupo);
         
    }
}
