package ni.edu.ucem.webapi.web.inventario;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Paginacion;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import org.springframework.web.context.request.WebRequest;

@RestController
@RequestMapping("/v1/inventario/cuartos")
public class CuartoResource 
{
    private final InventarioServiceImpl inventarioService;
    
    @Autowired
    public CuartoResource(final InventarioServiceImpl inventarioService)
    {
        this.inventarioService = inventarioService;
    }
    
    public static boolean isNumeric(String str)  
    {  
      try  
      {  
        double d = Double.parseDouble(str);  
      }  
      catch(NumberFormatException nfe)  
      {  
        return false;  
      }  
      return true;  
    }
    
    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public ListApiResponse<Cuarto> obtenerCuartos(
            @RequestParam(value = "fields", required = false, defaultValue="")  String fields,
            @RequestParam(value = "id", required = false, defaultValue="")  String id,
            @RequestParam(value = "numero", required = false, defaultValue="")  String numero,
            @RequestParam(value = "descripcion", required = false, defaultValue="")  String searchDescripcion,
            @RequestParam(value = "categoria", required = false, defaultValue="") final String categoria,
            @RequestParam(value = "search", required = false, defaultValue="")  String search,
            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="0") final Integer limit,
            @RequestParam(value = "sort", required = false, defaultValue="")  String sort,
            @RequestParam(value = "sortOrder", required = false, defaultValue="")  String sortOrder)
            
    {
        final Paginacion paginacion = new Paginacion.Builder(offset, limit).build();
        String whereFilter = " where 1 = 1 ";
        String[] campos = fields.split(",");
        String columnas = "";
        int entra = 0;
        Pagina<Cuarto> pagina;
       
       //  System.out.print("categoria = " + categoria); 

        if (!"".equals(id) && isNumeric(id)){
             whereFilter = whereFilter + " and id = " + id;
         }
        
         if (!"".equals(categoria) && isNumeric(categoria)){
             whereFilter = whereFilter + " and categoria = " + categoria;
         }

          if (!"".equals(numero) && isNumeric(numero)){
             whereFilter = whereFilter + " and numero = " + numero;
         }
         
         if (!"".equals(searchDescripcion)){
             whereFilter = whereFilter + " and descripcion = '" + searchDescripcion + "'";
         } 

         if (!"".equals(search)){
             whereFilter = whereFilter +  " and ( descripcion like '%" + search + "%'";

             if (isNumeric(search)) {
                  whereFilter = whereFilter +  " or numero = " + search + " or categoria = " + search;
             }
              whereFilter = whereFilter +  ")";
         }

         if (!"numero".equals(sort) && !"descripcion".equals(sort) && !"categoria".equals(sort) && !"".equals(sort)){
             //throw new IllegalArgumentException("404 (Not Found) campo no encontado para " + sort);
              sort = "1";
         } 
         if ("".equals(sort) ) {
             sort = "1";
         } 

         if (!"asc".equals(sortOrder) && !"desc".equals(sortOrder) && !"".equals(sortOrder)) {
             //throw new IllegalArgumentException("404 (Not Found) parametro no encontrado para " + sortOrder);
              sortOrder = "asc";
         }

        if ("".equals(sortOrder) ) {
             sortOrder = "asc";
        } 
         
        if (campos.length <= 0) {
            columnas = " * ";
        }

        for (int i = 0; i < campos.length; i++){
         //System.out.println("campos[i] = " + campos[i]);
            if ("id".equals(campos[i]) || "numero".equals(campos[i]) ||  "descripcion".equals(campos[i]) || "categoria".equals(campos[i]) ) {
                if (entra == 1) {
                   columnas = columnas + " , "; 
                }
                entra = 1;
                columnas = columnas + campos[i];
            }
        }

         if (campos.length <= 0 || entra == 0) {
            columnas = " * ";
        }
         
         
         pagina = this.inventarioService.obtenerTodosCuarto(paginacion, sort, sortOrder, whereFilter, columnas);
        
        return new ListApiResponse<Cuarto>(Status.OK, pagina);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtener(@PathVariable("id") final int id, WebRequest webRequest)
    //        public ApiResponse obtener(@PathVariable("id") final int id)
    {
      final Cuarto cuarto = this.inventarioService.obtenerCuarto(id);
     /* if (webRequest.checkNotModified(cuarto.getModificado().getTime())) {
           return null;
        } */
        return new ApiResponse(Status.OK, cuarto);
    }
    
    /**
     * Para que la validación funcione solo es necesario agregar la anotación @valid al método.
     * Spring se asegura que la petición entrante sea validado con las reglas definidas en el POJO. 
     * @param cuarto
     * @param result
     * @return
     */
    // forma_1
    //@PreAuthorize ("hasRole('ADMIN')") // solo lo pueden usar los usuarios con rol administrador se enlaza con ConfigSeguridad con la anotacion @EnableGlobalMethodSecurity(prePostEnabled = true)
    @RequestMapping(method = RequestMethod.POST,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    
    public ApiResponse guardarCuarto(@Valid @RequestBody final Cuarto cuarto, BindingResult result) 
    {
        if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }
        
        this.inventarioService.agregarCuarto(cuarto);
        return new ApiResponse(Status.OK, cuarto);
    }
    
    /**
     * Negociación de contenido. Aceptamos form-parameters para la creación de un nuevo recurso.
     * @param numero
     * @param descripcion
     * @param categoria
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarCuartoConFormData(final Short numero, final String descripcion, final Integer categoria) 
    {
        Cuarto cuarto = new Cuarto(numero, descripcion, categoria);
        this.inventarioService.agregarCuarto(cuarto);
        return new ApiResponse(Status.OK, cuarto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT,
            produces="application/json")
    public ApiResponse guardarCuarto(@PathVariable("id") final int id, 
            @RequestBody final Cuarto cuartoActualizado) 
    {
        final Cuarto cuarto = new Cuarto(id,
                cuartoActualizado.getNumero(), 
                cuartoActualizado.getDescripcion(),
                cuartoActualizado.getCategoria());
        this.inventarioService.guardarCuarto(cuarto);
        return new ApiResponse(Status.OK, cuarto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE,
            produces="application/json")
    public ApiResponse eliminarCuarto(@PathVariable("id") final int id) 
    {
        final Cuarto cuarto = this.inventarioService.obtenerCuarto(id);
        this.inventarioService.eliminarCuarto(cuarto.getId());
        return new ApiResponse(Status.OK,null);
    }
}
