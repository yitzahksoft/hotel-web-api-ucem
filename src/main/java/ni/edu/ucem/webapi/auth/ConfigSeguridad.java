/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.auth;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author pc20
 */
@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true) // forma_1 esto es para implementar la seguridad a nivel de metodo
// se tiene q coloar en Resource la anotacion @PreAuthorize ("hasRole('ADMIN')") 
public class ConfigSeguridad extends WebSecurityConfigurerAdapter
{
    private final DataSource dataSource;
    //private final AuthenticationManagerBuilder auth;
    
    @Autowired
    public ConfigSeguridad(final DataSource dataSource){
        this.dataSource = dataSource;
        
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        
        http.authorizeRequests()
             .antMatchers(HttpMethod.GET).permitAll() // con esto todos los usuarios pueden hacer del metodo get
                                                        // es importante ponerlo antes de .anyRequest().authenticated()
                //.antMatchers(HttpMethod.GET "/v1/inventario/cuartos").permitAll()  // aplica solo para el recurso cuartos
             //.antMatchers(HttpMethod.POST, "/v1/inventario/cuartos").hasAnyRole("ADMIN") // esta es la forma global para q solo el usuario AMIN pueda agregar
               
                // permsiso de forma global forma_2
                //.antMatchers(HttpMethod.POST).hasAnyRole("ADMIN")
                //.antMatchers(HttpMethod.POST, "/v1/reservaciones").permitAll()
                .antMatchers(HttpMethod.POST, "/v1/inventario/cuartos", "/v1/inventario/categorias").hasAnyRole("ADMIN") // esta es la forma global para q solo el usuario AMIN pueda agregar
                //.antMatchers(HttpMethod.POST, "/v1/reservaciones").permitAll()
                .antMatchers(HttpMethod.DELETE).hasAnyRole("ADMIN")
                .antMatchers(HttpMethod.PUT).hasAnyRole("ADMIN")
                .anyRequest().authenticated()
        .and()
                
                .httpBasic()
        .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS) // con eso el servidor no guarda la sesion
        .and()
             .csrf().disable()
             .headers().cacheControl().disable();  // se desabilita el control automatico del cache
    }
     //https://www.base64encode.org/ -- para decodificar
    //ucem:1234 -> Encode: dWNlbToxMjM0
   @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.jdbcAuthentication().dataSource(dataSource)
        .passwordEncoder(new BCryptPasswordEncoder(10))
        .usersByUsernameQuery(
                "select username, password, enabled from usuarios where username = ?")
        .authoritiesByUsernameQuery(
                "select username, role from usuarios_roles where username = ?");
    }
}
