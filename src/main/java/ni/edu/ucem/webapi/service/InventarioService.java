package ni.edu.ucem.webapi.service;

import java.util.Date;

import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Paginacion;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.ReservacionCompuesta;

public interface InventarioService 
{
    public void agregarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto);

    public Integer guardarReservacion(final Reservacion pReservacion);

    public void eliminarCategoriaCuarto(int id);
    
    public CategoriaCuarto obtenerCategoriaCuarto(final int id);

    public Pagina<CategoriaCuarto> obtenerTodosCategoriaCuartos(final Paginacion paginacion, final String pSort, final String pSortOrder, final String pSearch, final String fields);

    public void agregarCuarto(final Cuarto pCuarto);

    public void guardarCuarto(final Cuarto pCuarto);

    public void eliminarCuarto(final int pCuarto);

    public Cuarto obtenerCuarto(final int pCuarto);

    public Pagina<Cuarto> obtenerTodosCuarto(final Paginacion paginacion, final String pSort, final String pSortOrder, final String pSearch, final String fields);

    public Pagina<Cuarto> obtenerTodosCuartoEnCategoria(final int pCategoriaCuarto, final Paginacion paginacion);
    
    public Cupo obtenerCuartosDisponibles(final String pSearch, final Date fechaI, final Date fechaF);
       
    public Huesped obtenerHuespedPorId(final int pId);
    
    public ReservacionCompuesta obtenerReservacionCompuesta(final int id);
}
