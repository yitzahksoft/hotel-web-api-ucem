/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.cache;
import javax.servlet.Filter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

/**
 *
 * @author pc20
 */
@Configuration
public class EtagConfig {
    @Bean // le dice a spring y q se ejecute al inicio de la ejecucion de la aplicacion
    public Filter etagFilter(){
        return new ShallowEtagHeaderFilter();
    }
}
