/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.modelo;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author jcalero
 */
public class Huesped {
    private Integer id;
    @NotEmpty(message = "El nombre es un dato requerido.")
    private String nombre;
    
    @NotEmpty(message = "El email es un dato requerido.")
    private String email;
    
    @NotEmpty(message = "El telefono es un dato requerido.")
    private String telefono;

     public Huesped()
    {
    }
    
    public Huesped(final Integer id, final String nombre, final String email, final String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }
    
    
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
