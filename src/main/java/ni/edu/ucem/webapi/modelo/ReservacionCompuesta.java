/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.modelo;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jcalero
 */

public class ReservacionCompuesta {
    private Integer id;
    
    @NotNull(message = "La fecha inicio es requerido")
    private Date  desde;
    
    @NotNull(message = "La fecha fin es requerido")
    private Date  hasta;
    
    @NotNull(message = "El cuarto es requerido.")
   // @Range(min = 1, max = 1000)
    private Cuarto cuarto;
    
    @NotNull(message = "El huesped es requerido.")
   // @Range(min = 1, max = 1000)
    private Huesped huesped;
    
    public ReservacionCompuesta () {
    }

    public ReservacionCompuesta (final Integer id, final Date desde, final Date hasta,
            final Cuarto cuarto, final Huesped huesped) {
        this.id = id;
        this.desde = desde;
        this.hasta = hasta;
        this.cuarto = cuarto;
        this.huesped = huesped;
    }
    
    public ReservacionCompuesta (final Date desde, final Date hasta,
            final Cuarto cuarto, final Huesped huesped) {
        this.desde = desde;
        this.hasta = hasta;
        this.cuarto = cuarto;
        this.huesped = huesped;
    }
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the desde
     */
    public String getDesde() {
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return formateador.format(desde);
        
    }

    /**
     * @param desde the desde to set
     */
    public void setDesde(Date desde) {
        this.desde = desde;
    }

    /**
     * @return the hasta
     */
    public String getHasta() {
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return formateador.format(hasta);
    }

    /**
     * @param hasta the hasta to set
     */
    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    /**
     * @return the cuarto
     */
    public Cuarto getCuarto() {
        return cuarto;
    }

    /**
     * @param cuarto the cuarto to set
     */
    public void setCuarto(Cuarto cuarto) {
        this.cuarto = cuarto;
    }

    /**
     * @return the huesped
     */
    public Huesped getHuesped() {
        return huesped;
    }

    /**
     * @param huesped the huesped to set
     */
    public void setHuesped(Huesped huesped) {
        this.huesped = huesped;
    }

    /**
     * @return the cuarto
     */
    
}
