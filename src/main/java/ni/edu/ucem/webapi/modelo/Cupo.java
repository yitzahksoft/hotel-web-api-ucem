/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.modelo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jcalero
 */
public class Cupo {
    private Date fechaIngreso;
    private Date fechaSalida;
    private List<Cuarto> cuartos;
           
    public Cupo()
    {
    }
    
    public Cupo(final Date fechaIngreso, final Date fechaSalida, final List<Cuarto> cuartos) {
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.cuartos = cuartos;
    }

    /**
     * @return the fechaIngreso
     */
    public String getFechaIngreso() {
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return formateador.format(fechaIngreso);
    }

    /**
     * @param fechaIngreso the fechaIngreso to set
     */
    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * @return the fechaSalida
     */
    public String getFechaSalida() {
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return formateador.format(fechaSalida);
        //return fechaSalida;
    }

    /**
     * @param fechaSalida the fechaSalida to set
     */
    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    /**
     * @return the cuartos
     */
    public List<Cuarto> getCuartos() {
        return cuartos;
    }

    /**
     * @param cuartos the cuartos to set
     */
    public void setCuartos(List<Cuarto> cuartos) {
        this.cuartos = cuartos;
    }

}
