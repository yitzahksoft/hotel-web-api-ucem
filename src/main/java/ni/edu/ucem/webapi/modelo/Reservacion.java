/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.modelo;

import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jcalero
 */
public class Reservacion {
    private Integer id;
    
    @NotNull(message = "La fecha inicio es requerido")
    private Date  desde;
    
    @NotNull(message = "La fecha fin es requerido")
    private Date  hasta;
    
    @NotNull(message = "El cuarto es requerido.")
   // @Range(min = 1, max = 1000)
    private Integer cuarto;
    
    @NotNull(message = "El huesped es requerido.")
   // @Range(min = 1, max = 1000)
    private Integer huesped;
    
    public Reservacion () {
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the desde
     */
    public Date getDesde() {
        return desde;
    }

    /**
     * @param desde the desde to set
     */
    public void setDesde(Date desde) {
        this.desde = desde;
    }

    /**
     * @return the hasta
     */
    public Date getHasta() {
        return hasta;
    }

    /**
     * @param hasta the hasta to set
     */
    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    /**
     * @return the cuarto
     */
    public Integer getCuarto() {
        return cuarto;
    }

    /**
     * @param cuarto the cuarto to set
     */
    public void setCuarto(Integer cuarto) {
        this.cuarto = cuarto;
    }

    /**
     * @return the huesped
     */
    public Integer getHuesped() {
        return huesped;
    }

    /**
     * @param huesped the huesped to set
     */
    public void setHuesped(Integer huesped) {
        this.huesped = huesped;
    }
}
