/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.daoImpl;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Reservacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jcalero
 */
@Repository
public class ReservacionDAOImpl implements ReservacionDAO {

     private final JdbcTemplate jdbcTemplate;
    
    @Autowired
    public ReservacionDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }
     
    @Override
    public Reservacion obtenerPorId(int pId) {
         String sql = "select * from reservacion where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }

    @Override
    public int contar(String pSearch) {
         final String sql = "select count(*) from reservacion " + pSearch;
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public List<Reservacion> obtenerTodos(int offset, int limit, String sort, String sortOrder, String search, String pFields) {
          String sql = "select " + pFields + " from reservacion " + search + " order by " + sort + " " + sortOrder +  " offset ? limit ? ";
       System.out.println("sql = " + sql);
       return this.jdbcTemplate.query(sql, new Object[]{offset, limit},
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }

    @Override
    public List<Reservacion> obtenerTodosDisponible(int offset, int limit, String sort, String sortOrder, String search, String pFields) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional
    @Override
    public Integer agregar(Reservacion pReservacion) {
        Integer idCuarto, idFinal, idHusped; 
        String sql;
       //  KeyHolder keyHolder = new GeneratedKeyHolder();
      //SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
       
       sql = "select max(cuarto) from reservacion where desde >= '" + formateador.format(pReservacion.getDesde()) 
               + "' and hasta <= '" + formateador.format(pReservacion.getHasta())+ "' and cuarto = " + pReservacion.getCuarto();
       idCuarto = this.jdbcTemplate.queryForObject(sql, Integer.class);
       System.out.print(" idInicial = " + idCuarto);
       
       if (idCuarto == null)
       {
                
               sql = "select max(id) from cuarto where id = " + pReservacion.getCuarto();
               idCuarto = this.jdbcTemplate.queryForObject(sql, Integer.class);
           
               if (idCuarto == null){
                   throw new IllegalArgumentException("El cuarto con Id = " +  pReservacion.getCuarto() + " no existe");
               }
               
               sql = "select max(id) from huesped where id = " + pReservacion.getHuesped();
               idHusped = this.jdbcTemplate.queryForObject(sql, Integer.class);
               
               if (idHusped == null){
                   throw new IllegalArgumentException("El Huesped con Id = " +  pReservacion.getHuesped() + " no existe");
               }
               
                sql = new StringBuilder()
                     .append("INSERT INTO reservacion")
                     .append(" ")
                     .append("(desde, hasta, cuarto, huesped)")
                     .append(" ")
                     .append("Values(?, ?, ?, ?)")
                     .toString();
             final Object[] parametros = new Object[4];
             parametros[0] = pReservacion.getDesde();
             parametros[1] = pReservacion.getHasta();
             parametros[2] = pReservacion.getCuarto();
             parametros[3] = pReservacion.getHuesped();
             this.jdbcTemplate.update(sql,parametros);

            sql = "select max(id) from reservacion ";
            idFinal = this.jdbcTemplate.queryForObject(sql, Integer.class);
            return idFinal;
       }else{
            return 0;
        }
    }

    @Override
    public void guardar(Reservacion pReservacion) {
        final String sql = new StringBuilder()
                .append("UPDATE reservacion")
                .append(" ")
                .append("set desde = ?")
                .append(",hasta = ?")
                .append(",cuarto = ?")
                .append(",huesped = ?")
                .append(" ")
                .append("where id = ?")
                .toString();
        final Object[] parametros = new Object[5];
        parametros[0] = pReservacion.getDesde();
        parametros[1] = pReservacion.getHasta();
        parametros[2] = pReservacion.getCuarto();
        parametros[3] = pReservacion.getHuesped();
        parametros[4] = pReservacion.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(int pId) {
        final String sql = "delete from reservacion where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }
    
}
