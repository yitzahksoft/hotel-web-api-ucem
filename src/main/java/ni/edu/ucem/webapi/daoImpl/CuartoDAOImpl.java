package ni.edu.ucem.webapi.daoImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;

@Repository
public class CuartoDAOImpl implements CuartoDAO 
{
    private final JdbcTemplate jdbcTemplate;
   
    @Autowired
    public CuartoDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Cuarto obtenerPorId(final int pId) 
    {
        String sql = "select * from cuarto where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }
    
    @Override
    public Cuarto obtenerEspecifico(final String pSearch) {
          String sql = "select * from cuarto " + pSearch ;
       System.out.println("sql = " + sql);
       return this.jdbcTemplate.queryForObject(sql, 
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }
    
    @Override
    public int contar(final String pSearch)
    {
        final String sql = "select count(*) from cuarto " + pSearch;
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }
    
    @Override
    public int contarPorCategoria(final int categoriaId)
    {
       //  Integer intObj = new Integer(categoriaId);
        final String sql = "select count(*) from cuarto where categoria = " + categoriaId;
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public List<Cuarto> obtenerTodos(final int pOffset, final int pLimit, String pSort, String pSortOrder, final String pSearch, String pFields ) 
    {
              
       String sql = "select " + pFields + " from cuarto " + pSearch + " order by " + pSort + " " + pSortOrder +  " offset ? limit ? ";
       System.out.println("sql = " + sql);
       return this.jdbcTemplate.query(sql, new Object[]{pOffset, pLimit},
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }

    @Override
    public List<Cuarto> obtenerTodosPorCategoriaId(int pCategoriaId, int pOffset, int pLimit) 
    {
        final String sql = "select * from cuarto where categoria = ? offset ? limit ?";
        return this.jdbcTemplate.query(sql, new Object[]{pCategoriaId, pOffset, pLimit},
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }

    @Override
    public void agregar(final Cuarto pCuarto) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO cuarto")
                .append(" ")
                .append("(numero, descripcion, categoria)")
                .append(" ")
                .append("VALUES (?, ?, ?)")
                .toString();
        final Object[] parametros = new Object[3];
        parametros[0] = pCuarto.getNumero();
        parametros[1] = pCuarto.getDescripcion();
        parametros[2] = pCuarto.getCategoria();
        this.jdbcTemplate.update(sql,parametros);
        
    }

    @Override
    public void guardar(final Cuarto pCuarto) 
    {        
      // SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        final String sql = new StringBuilder()
                .append("UPDATE cuarto")
                .append(" ")
                .append("set numero = ?")
                .append(",descripcion = ?")
                .append(",categoria = ?")
             //  .append(",modificado = ?")
                .append(" ")
                .append("where id = ?")
                .toString();
        /*final Object[] parametros = new Object[5];
        parametros[0] = pCuarto.getNumero();
        parametros[1] = pCuarto.getDescripcion();
        parametros[2] = pCuarto.getCategoria();
        parametros[3] = formateador.format(new Date()) ;
        parametros[4] = pCuarto.getId();*/
        
        final Object[] parametros = new Object[4];
        parametros[0] = pCuarto.getNumero();
        parametros[1] = pCuarto.getDescripcion();
        parametros[2] = pCuarto.getCategoria();
        parametros[3] = pCuarto.getId();
        
        
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from cuarto where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }

    @Override
    public List<Cuarto> obtenerCuartosDisponibles(String search) {
        String sql = "select * from cuarto " + search ;
       System.out.println("sql = " + sql);
       return this.jdbcTemplate.query(sql, 
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }

    
}
